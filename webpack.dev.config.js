const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const CSSModuleLoader = {
  loader: 'css-loader',
  options: {
    modules: true,
    sourceMap: true,
    modules: {
      localIdentName: '[local]__[hash:base64:5]',
    },
  },
};

const BabelLoader = {
  loader: 'babel-loader',
  options: {
    presets: [
      '@babel/preset-env',
      {
        plugins: ['babel-plugin-transform-class-properties',
          ['react-css-modules', {
            webpackHotModuleReloading: true,
          }],
        ],
      },
    ],
  },
};

module.exports = {
  mode: 'development',
  entry: './src/local.js',
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: BabelLoader,
      },
      {
        test: /\.css$/,
        exclude: /node_modules/,
        use: [
          { loader: 'style-loader' },
          CSSModuleLoader,
        ],
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'fonts/',
            },
          }],
      },
      {
        test: /\.svg/,
        exclude: /node_modules/,
        use: {
          loader: 'svg-url-loader',
          options: { iesafe: true, stripdeclarations: true },
        },
      },
    ],
  },
  resolve: {
    extensions: ['*', '.js', '.jsx'],
  },
  output: {
    path: `${__dirname}/`,
    publicPath: '/',
    filename: 'bundle.js',
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new CopyWebpackPlugin([{ from: 'src/assets/', to: '/assets/' }]),
  ],
  devServer: {
    contentBase: './src',
    hot: true,
  },
};
