export const MOCKED_PRODUCTS = [
  {
    title: 'Aviator',
    description: 'this product has a description',
    enabled: true,
    id: 123123123,
    material: 'glass',
    price: 200,
    brand: 'd&g',
    shape: 'circular',
    size: 'narrow',
    color: 'black',
    image: '../assets/images/thumbnails/aviator.jpg',
    skus: [{
      sku: 11,
      color: '#000',
      name: 'black',
      currentStockLevel: 20,
    },
    {
      sku: 11,
      color: '#fff',
      name: 'white',
      currentStockLevel: 20,
    }],

  },
  {
    title: 'Aviator',
    description: 'this product has a description',
    enabled: true,
    id: 13134233133,
    material: 'glass',
    price: 150,
    brand: 'd&g',
    shape: 'circular',
    size: 'small',
    color: 'NUDE',
    image: '../assets/images/thumbnails/magnus.jpg',
    skus: [{
      sku: 11,
      color: '#000',
      name: 'white',
      currentStockLevel: 20,
    },
    {
      sku: 11,
      color: '#ff0000',
      name: 'red',
      currentStockLevel: 20,
    }],

  },
  {
    title: 'Aviator',
    description: 'this product has a description',
    enabled: true,
    id: 13124243133,
    material: 'plastic',
    price: 300,
    brand: 'd&g',
    shape: 'circular',
    size: 'small',
    color: 'black',
    image: '../assets/images/thumbnails/muse.jpg',
    skus: [{
      sku: 11,
      color: '#0000ff',
      name: 'blue',
      currentStockLevel: 20,
    },
    {
      sku: 11,
      color: '#ff0000',
      name: 'red',
      currentStockLevel: 20,
    }],

  },
  {
    title: 'Aviator',
    description: 'this product has a description',
    enabled: true,
    id: 13124233134,
    material: 'glass',
    price: 100,
    brand: 'd&g',
    shape: 'rectangluar',
    color: 'black',
    size: 'narrow',
    image: '../assets/images/thumbnails/hsp.jpg',
    skus: [{
      sku: 11,
      color: '#000',
      colorName: 'blue',
      currentStockLevel: 20,
    },
    {
      sku: 11,
      color: '#000',
      colorName: 'black',
      currentStockLevel: 20,
    }],

  },
  {
    title: 'Aviator',
    description: 'this product has a description',
    enabled: true,
    id: 13124233153,
    material: 'plastic',
    price: 500,
    brand: 'd&g',
    shape: 'circular',
    size: 'narrow',
    color: 'black',
    image: '../assets/images/thumbnails/magnus.jpg',
    skus: [{
      sku: 11,
      color: '#000',
      colorName: 'black',
      currentStockLevel: 20,
    },
    {
      sku: 11,
      color: '#fff',
      colorName: 'white',
      currentStockLevel: 20,
    }],

  },
  {
    title: 'Aviator',
    description: 'this product has a description',
    enabled: true,
    id: 13155233133,
    material: 'glass',
    price: 50,
    brand: 'd&g',
    shape: 'circular',
    color: 'black',
    size: 'big',
    image: '../assets/images/thumbnails/hsp.jpg',
    skus: [{
      sku: 11,
      color: '#fff',
      colorName: 'white',
      currentStockLevel: 20,
    },
    {
      sku: 11,
      color: '#ff0000',
      colorName: 'red',
      currentStockLevel: 20,
    }],

  },
  {
    title: 'Aviator',
    description: 'this product has a description',
    enabled: true,
    id: 13124233133,
    material: 'wood',
    price: 487,
    brand: 'd&g',
    shape: 'circular',
    color: 'black',
    size: 'big',
    image: '../assets/images/thumbnails/aviator.jpg',
    skus: [{
      sku: 11,
      color: '#000',
      colorName: 'black',
      currentStockLevel: 20,
    },
    {
      sku: 11,
      color: '#fff',
      colorName: 'white',
      currentStockLevel: 20,
    }],

  },
];


export const FILTERS_GROUPS = {
  size: ['narrow', 'Big', 'small', 'medium'],
  material: ['glass', 'plastic', 'wood'],
  shape: ['square', 'round', 'rectangular'],
  brand: ['D&G', 'Rayban', 'Gant', 'Tiger of Sweden', 'Gucci', 'Prada', 'Tommy Hilfiger'],
  color: [{
    hex: '#654321',
    name: 'MARBLE',
  },
  {
    hex: '#534C47',
    name: 'CHARCOAL',
  },

  {
    hex: '#FFC1B3', name: 'PASTEL',
  },
  {
    hex: '#C04034', name: 'RED',
  },
  {
    hex: '#D4CAB0', name: 'NUDE',
  },
  {
    hex: '#0600FE', name: 'BLUE',
  },
  {
    hex: '#CEC9CD', name: 'TRANSPARENT',
  },
  {
    hex: '#98532F', name: 'TORTOISE',
  }],
};
