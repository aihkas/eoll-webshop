import React from 'react';
import ColorVariant from '../ColorVariant';

import s from './styles.css';

class Product extends React.PureComponent {
  render() {
    const { item } = this.props;

    return (
      <div className={s.productContainer}>
        <div className={s.hoverWrapper}>
          <img
            src={item.image}
            alt="Product"
            className={s.productImage}
          />
          <div className={s.hoverWrapper_text}>
            <div className={s.colorsContainer}>
                            Available In
              <div className={s.productColors_items}>
                {item.skus.map((color) => <ColorVariant key={color.hex} item={color} />)}
              </div>
            </div>
          </div>
        </div>
        <div />
        <div className={s.productName}>{item.title}</div>
        <span className={s.productBrand}>{item.brand}</span>
        <span className={s.productPrice}>
          {item.price}
        </span>
      </div>
    );
  }
}

export default Product;
