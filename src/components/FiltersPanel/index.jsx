import React, { PureComponent, Fragment } from 'react';
import Fade from '@material-ui/core/Fade';

import CheckboxFilterGroup from '../CheckboxFilterGroup';
import Slider from '../Slider';
import ColorsFilter from '../ColorsFilter';
import s from './styles.css';

class FiltersPanel extends PureComponent {
  render() {
    const { filters } = this.props;
    return (
      <>
        <div className={s.mobileMenu}>
          <span className={s.filtersTitle}>
                        FILTERS
            <img alt="filters menu" className={s.menuIcon} src="../assets/icons/arrow.svg" />
          </span>
        </div>
        <Fade
          appear
          in
          mountOnEnter
          unmountOnExit
          timeout={{
            appear: 1000,
            enter: 1000,
            exit: 4000,
          }}
        >
          <div className={s.filtersPanel}>
            <CheckboxFilterGroup options={filters.material} group="material" />
            <CheckboxFilterGroup options={filters.shape} group="shape" />
            <CheckboxFilterGroup options={filters.size} group="size" />
            <ColorsFilter options={filters.color} />
            <CheckboxFilterGroup options={filters.brand} group="brand" />
            <div className={s.priceRange}>
                            Price range
              <Slider defaultValue={[0, 100]} />
            </div>
          </div>
        </Fade>
      </>
    );
  }
}

export default FiltersPanel;
