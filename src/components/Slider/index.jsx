import React from 'react';
import { Slider as MaterialSlider, withStyles } from '@material-ui/core';
import _debounce from 'lodash.debounce';

import useStore from '../Store';

const styles = {
  root: {
    color: '#000', opacity: 1, height: 3, padding: '13px 0',
  },
  thumb: {
    height: 12,
    width: 12,
    backgroundColor: '#fff',
    border: '1px solid rgba(0,0,0,0.8)',
    marginTop: -5,
    marginLeft: -5,
    boxShadow: 'none',
    '&:focus,&:hover,&$active': { boxShadow: 'none' },
    '& .bar': {
      height: 9, width: 1, backgroundColor: 'currentColor', marginLeft: 1, marginRight: 1,
    },
  },
  active: {},
  valueLabel: {
    left: 'calc(-50% - 5px)',
    top: 20,
    '& *': {
      background: 'transparent',
      color: '#000',
    },

  },
  track: {
    height: 3, opacity: 0.1,
  },
  rail: {
    color: '#000', opacity: 0.1, height: 3,
  },
};

function Slider(props) {
  const [{ priceRange }, { setPriceRange }] = useStore();
  const handleChange = _debounce((event, newValue) => { setPriceRange(newValue); }, 500);
  return <MaterialSlider {...props} onChange={handleChange} valueLabelDisplay="on" valueLabelFormat={(x) => `$ ${5 * x}`} />;
}

export default withStyles(styles)(Slider);
