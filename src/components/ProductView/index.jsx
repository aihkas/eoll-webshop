import React, { useState } from 'react';

import { useLocation, useParams } from 'react-router-dom';
import Slide from '@material-ui/core/Slide';
import Fade from '@material-ui/core/Fade';

import { MOCKED_PRODUCTS } from '../../utils/constants';
import useStore from '../Store';
import ColorVariant from '../ColorVariant';
import s from './styles.css';

export default function ProductView() {
  const [added, setAdded] = useState(false);
  const [selected, setSelected] = useState(false);
  const [selectedItem, selectItem] = useState({});
  const [{ cartItems }, { addToCart }] = useStore();

  const item = useLocation().state || MOCKED_PRODUCTS[useParams().id];

  const addProduct = () => {
    // TODO: add a color cariant of the selected item
    addToCart(item);
    setAdded(true);
  };

  const onSelect = () => {
    setAdded(false);
    setSelected(!selected);
  };

  return (
    <div className={s.productViewContainer}>
      <Slide direction="right" appear in>
        <div className={s.expandedProductImage} timeout={3000}>
          <img
            src={item.image}
            alt="Product"
          />
          <div className={s.imageButtonsContainer}>
            <button type="button">
              <img alt="previous" className={s.leftArrow} src="../../assets/icons/arrow.svg" />
            </button>
            <button type="button">
              <img alt="next" className={s.rightArrow} src="../../assets/icons/arrow.svg" />
            </button>
          </div>
        </div>
      </Slide>
      <Fade direction="right" appear in timeout={1000}>
        <div className={s.productInfo}>
          <div className={s.title}>{item.title}</div>
          <span className={s.brand}>{item.brand}</span>
          <span className={s.price}>
            {item.price}
          </span>
          <div className={s.colors}>
            <span className={s.colorsHeading}> Select Colors</span>
            <div className={s.colorsButtons}>

              {item.skus.map((color) => (
                <div onClick={onSelect}>
                  <ColorVariant item={color} displayTitles />
                </div>
              ))}
            </div>
          </div>
          <div className={s.buttonContainer}>
            {
              added ? (
                <button type="button" className={s.button_success}>
                  <img src="../../assets/icons/checkmark.svg" />
                                </button>
              )
                : <button type="button" className={s.button_add} onClick={addProduct} disabled={!selected}> Add to cart </button>
}
          </div>
          <span className={s.descriptionHeader}>
            {item.description}
          </span>
          <hr className={s.separator} />
          <span className={s.descriptionHeader}>Material</span>
          <span className={s.descriptor}>{item.material}</span>
          <span className={s.descriptionHeader}>Shape</span>
          <span className={s.descriptor}>{item.shape}</span>
          <span className={s.descriptionHeader}>Dimension</span>
          <span className={s.descriptor}>{item.size}</span>
          <button type="button" className={s.similarProducts}>See Similar Products</button>
        </div>
      </Fade>
    </div>
  );
}
