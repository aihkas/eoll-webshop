import React, { PureComponent } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import Header from './Header';
import ProductList from './ProductList';
import ProductView from './ProductView';
import s from '../utils/StyleSettings.css';

export default class App extends PureComponent {
  render() {
    return (
      <Router>
        <div className={s.app}>
          <Header />
          <Switch>
            <Route exact path="/" component={ProductList} />
            <Route path="/product/:id" component={ProductView} />
          </Switch>
        </div>
      </Router>
    );
  }
}
