import _reject from 'lodash.reject';

export const addToCart = (store, product) => {
  store.setState({ cartItems: [...store.state.cartItems, product] });
};

export const setPriceRange = (store, range) => {
  const priceRange = [range[0] * 5, range[1] * 5];
  store.setState({ priceRange });
};

export const selectItem = (store, selectedItem) => {
  store.setState({ selectedItem });
};

export const addFilter = (store, filter) => {
  const { group, id } = filter;
  store.setState({ filters: [...store.state.filters, { [group]: id }] });
};

export const removeFilter = (store, filter) => {
  let { filters } = store.state;
  const { group, id } = filter;
  filters = _reject(filters, { [group]: id });
  store.setState({ filters });
};
