import React from 'react';
import useGlobalHook from 'use-global-hook';
import * as actions from './actions';

const initialState = {
  cartItems: [],
  filters: [],
  priceRange: [0, 10000],
  selectedItem: {},
};

const useStore = useGlobalHook(React, initialState, actions);

export default useStore;
