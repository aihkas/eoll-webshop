import React from 'react';
import s from './styles.css';

const ColorVariant = (props) => {
  const { item, displayTitles = false } = props;
  return (
    <div className={s.variantContainer}>
      <div className={s.circle} style={{ background: item.color }} />
      {displayTitles && <span className={s.name}>{item.name}</span>}
    </div>
  );
};
export default ColorVariant;
