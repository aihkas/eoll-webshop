import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import Fade from '@material-ui/core/Fade';
import _isEqual from 'lodash.isequal';

import useStore from '../Store';
import FiltersPanel from '../FiltersPanel';
import Product from '../Product';
import { FILTERS_GROUPS, MOCKED_PRODUCTS } from '../../utils/constants';
import s from './styles.css';


const ProductsList = () => {
  const [storeState] = useStore();
  const { filters, priceRange } = storeState;
  const products = MOCKED_PRODUCTS;

  const applyPriceFilter = (product) => {
    return product.price >= priceRange[0] && product.price <= priceRange[1];
  };

  const shouldDisply = (product) => {
    if (filters.length === 0) { return true; }

    return filters.some((filter) => {
      const group = Object.keys(filter)[0];
      return _isEqual(filter[group], product[group]);
    });
  };

  const renderProduct = (item) => ((shouldDisply(item) && applyPriceFilter(item))
    ? (
      <Grid item xs={12} sm={6} md={6} lg={4}>
        <Link to={{ pathname: `/product/${item.id}`, state: item }}>

          <Product item={item} key={item.id} />
        </Link>

      </Grid>
    ) : null);

  return (
    <>
      <FiltersPanel filters={FILTERS_GROUPS} />

      <Fade
        appear
        in
        mountOnEnter
        timeout={{
          appear: 1000,
          enter: 1000,
          exit: 4000,
        }}
      >
        <div className={s.productListWrapper}>
          <Grid container spacing={6}>
            {products.map(renderProduct)}
          </Grid>
        </div>
      </Fade>
    </>
  );
};

export default ProductsList;
