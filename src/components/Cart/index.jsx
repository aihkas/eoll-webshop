import React from 'react';
import {Badge as MaterialBadge, withStyles} from '@material-ui/core';
import useStore from '../Store';

const Badge = withStyles(theme => ({
  badge: {
    backgroundColor: '#6AD0C8',
  },
}))(MaterialBadge);

const Cart = () => {
  const [{ cartItems }] = useStore();

  return (
    <Badge badgeContent={cartItems.length} color="primary">
      <img src="../assets/icons/cart.svg" />
    </Badge>
  );
};

export default Cart;
