import React, { useState } from 'react';

import useStore from '../Store';
import s from './styles.css';

const ColorsFilter = (props) => {
  const [selected, setSelected] = useState(false);
  const { color, displayTitles = false, options } = props;
  const [globalState, actions] = useStore();

  const handleSelect = (event) => {
    const group = 'color';
    const id = event.target.dataset.colorName;
    selected ? actions.addFilter({ group, id }) : actions.removeFilter({ group, id });
    setSelected(!selected);
  };

  const renderColorButton = (item) => (
    <div key={item.hex} className={s.container} onClick={handleSelect}>
      <div className={s.circle} data-color-name={item.name} style={{ background: item.hex }} />
      <span>{item.name}</span>
    </div>
  );

  return (
    <div className={s.colorFilters_container}>
      <span className={s.FilterGroup_title}>Colors</span>
      <div className={s.colorsFiltersWrapper}>
        {options.map((item) => renderColorButton(item))}
      </div>
    </div>
  );
};
export default ColorsFilter;
