import React from 'react';

import Hidden from '@material-ui/core/Hidden';
import Fab from '@material-ui/core/Fab';
import Cart from '../Cart';

import s from './styles.css';

const Header = () => (
  <div className={s.container}>
    <h1 className={s.logo}> EOLL </h1>
    <Hidden xsDown>
      <div className={s.sectionsContainer}>
        <span>LADIES</span>
        <span>GENTS</span>
        <span>CHILDREN</span>
        <span>TRENDS</span>
        <span>SALE</span>
      </div>
      <div className={s.utilsContainer}>
        <span>Login</span>
        <span>Help</span>
        <Cart />
      </div>
    </Hidden>
    <Hidden smUp>
      <img className={s.hamburgerMenu} src="../assets/icons/hamburger.svg" />
      <div className={s.mobileCart}>
        <Fab>
          <Cart />
        </Fab>
      </div>
    </Hidden>
  </div>
);

export default Header;
