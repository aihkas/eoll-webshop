import React from 'react';

import useStore from '../Store';
import s from './styles.css';

const CheckboxFilterGroup = (props) => {
  const { group } = props;
  const [globalState, actions] = useStore();

  const handleChange = (e) => {
    const { id } = e.target;
    const { checked } = e.target;

    checked ? actions.addFilter({ group, id }) : actions.removeFilter({ group, id });
  };

  return (
    <div className={s.filterGroup}>
      <span className={s.filterGroup_title}>{props.group}</span>
      {props.options.map((item) => (
        <div className={s.checkboxWrapper} key={item}>
          <input
            type="checkbox"
            onChange={handleChange}
            id={item}
            name={item}
            className={s.customCheckbox}
          />
          <span className={s.customCheckbox_span} />
          <label htmlFor={item} className={s.customCheckbox_label}>
            {item}
          </label>
        </div>
      ))}
    </div>
  );
};

export default CheckboxFilterGroup;
